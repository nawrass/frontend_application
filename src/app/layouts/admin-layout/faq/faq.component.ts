import { Component, OnInit } from '@angular/core';
import {FaqService} from "../services/faq.service";
import {Question} from "../entity/question";
import {TokenStorageService} from "../auth/token-storage.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {DeletDocumentComponent} from "../delet-document/delet-document.component";
import {DeleteQuestionComponent} from "../delete-question/delete-question.component";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {EditDocumentComponent} from "../edit-document/edit-document.component";
import {EditeQuestionComponent} from "../edite-question/edite-question.component";
import {User} from "../entity/user";

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
    questionn:Question[]
    private roles: any[];
    private authority: any;
    id_user: number = this.tokenStorage.getId();
    user:User[];
    questions : Question=new Question();
    oppoSuitsForm_supprimer = this.fb.group({
        name: ['']
    })
    myFiles: any = []
    
    constructor(private faqService :FaqService,private tokenStorage: TokenStorageService,public dialog: MatDialog,public fb: FormBuilder) { }

  ngOnInit() {
      this. getAllQuestion();
      this.roles = this.tokenStorage.getAuthorities()


      console .log('roless '+ JSON.stringify(this.roles))
      console .log('tokenstorage '+ JSON.stringify(this.tokenStorage))
      this.roles.forEach(authority => {
          console.log('test1 ' + JSON.stringify(authority.authority))
          if (authority.authority === 'ROLE_ADMIN') {
              console.log('test2 ' + JSON.stringify(authority.authority))
              this.authority = 'admin';
              return true

          }
          else this.authority = 'user';
          return false;


      });
      console .log('the name is'+ JSON.stringify(this.authority))

  }

  getAllQuestion(){

   return this.faqService.getAllQuestion().subscribe(
        response => {
          this.questionn = response;
          console.log("liste"+this.questionn)
            this.getIdDoc();
            console.log("myliste" + JSON.stringify(this.myFiles))
        },
        err => console.log(err)
    );
  }
  createQuestion(){
      this.faqService.createQuestion(this.questions ,this.id_user).subscribe(
          response => {
              console.log("liste"+response)
              this.questions=new Question();
          },
          (err) => {
              console.warn("an error has occured", err)
          },

          () => {
              this.getAllQuestion();
          }
      );

  }
    deleteQuestion() {
        let formData = new FormData();
        for (let i = 0; i < this.myFiles.length; i++) {
            if (this.myFiles[i].selected == true) {
        this.faqService.deleteQuestion(this.myFiles[i].id)
            .subscribe(
                data => {
                    console.log(data);
                },
                (err) => {
                    console.warn("an error has occured", err)
                },
                () => {
                    this.getAllQuestion();
                }
            );
            }
        }
    }
    checkDoc(event, id: number) {
        console.log("id ,"+this.myFiles.length)
        for (let i = 0; i < this.myFiles.length; i++) {
            console.log("id0"+i)
            if (id == this.myFiles[i].id) {
                console.log("id2"+id)
                this.myFiles[i].selected = event.checked;
                console.log("select" + JSON.stringify(this.myFiles[i]));
                break
            }
        }

    }
    getIdDoc() {
        this.myFiles = [];
        for (let i = 0; i < this.questionn.length; i++) {

                const fileObject = {
                    id: this.questionn[i].id,
                    question: this.questionn[i].question,

                    selected: false
                }

            this.myFiles.push(fileObject)
        }
    }
    // editeQuestion(){
    //     for (let i = 0; i < this.myFiles.length; i++) {
    //         if (this.myFiles[i].selected == true) {
    //             this.faqService.editeQuetion(this.question,this.myFiles[i].id)
    //                 .subscribe(
    //                     data => {
    //                         console.log(data);
    //                     },
    //                     (err) => {
    //                         console.warn("an error has occured", err)
    //                     },
    //                     () => {
    //                         this.getAllQuestion();
    //                     }
    //                 );
    //         }
    //     }
    // }

    editQuestion(doc: any) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        const dialogRef = this.dialog.open(EditeQuestionComponent, {
                data: {
                    doc: doc
                }
            }
        );
        dialogRef.afterClosed().subscribe(result => {
            this.getAllQuestion();
        });
        console.log("idedit"+JSON.stringify(doc))
    }
question:string;
    searchNomQuestion() {
        console.log("getnom:" + this.question)
        if (this.question == "") {
            this.faqService.getAllQuestion().subscribe(
                response => {

                    this.questionn = response;

                },
                err => console.log(err),
            );
        }
        this.faqService.searchQuestion(this.question).subscribe(
            (response: any) => {

                this.questionn = response;
                // console.log("doc"+this.document)
            },
            err => console.log(err),
        );
    }
}
