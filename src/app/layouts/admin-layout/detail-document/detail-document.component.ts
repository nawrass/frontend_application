import {Component, Inject, OnInit} from '@angular/core';
import {Document} from "../entity/document";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {RepertoireService} from "../services/repertoire.service";
import {NotificationService} from "../services/notification.service";
import {Repertoire} from "../entity/repertoire";
import {DocumentService} from "../services/document.service";
import {FormBuilder, FormGroup} from '@angular/forms';
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {HttpClient} from "@angular/common/http";
import {tap} from "rxjs/operators";
import {TokenStorageService} from "../auth/token-storage.service";

@Component({
    selector: 'app-detail-document',
    templateUrl: './detail-document.component.html',
    styleUrls: ['./detail-document.component.scss']
})
export class DetailDocumentComponent implements OnInit {
    repertoire: Repertoire;
    document: Document[];
    // documents :any
    codeRepertoires: Array<String> = [];
    datalocalURL: any;
    id_user: number = this.token.getId();

    constructor(private token: TokenStorageService, public dialog: MatDialog,
                private formBuilder: FormBuilder,
                public repertoireservice: RepertoireService,
                private documentservice: DocumentService, private httpClient: HttpClient,
                @Inject(MAT_DIALOG_DATA) private data: any, public sanitizer: DomSanitizer,
                private dialogRef: MatDialogRef<DetailDocumentComponent>) {
    }

    newdoc: any;
    extension: any;
    documents: Document = new Document();
    url: string;
    urlSafe: SafeResourceUrl;

    ngOnInit() {
        this.documents = this.data.doc;
        this.url = this.data.doc.nomDoc;
        this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
        console.log("datadoc" + JSON.stringify(this.data.doc));
        this.repertoire = this.data.doc;
        this.extension = this.documents.nomDoc.substr((this.documents.nomDoc.lastIndexOf('.') + 1))

        this.documentservice.detailsContent(this.data.doc.id).pipe(tap(byteArray => {
            // @ts-ignore
            let newBlob = new Blob([byteArray], {type: this.data.doc.type});
            this.datalocalURL = window.URL.createObjectURL(newBlob);
            document.querySelector("iframe").src = this.datalocalURL;
        })).subscribe();

    }

    onNoClick(): void {
        this.dialogRef.close();
    }

// detailDocument(){
//
// this.getAllRepertoire();
// }
    getAllDocument() {
        this.documentservice.getAllDocument(this.id_user).subscribe(
            (response: any) => {

                this.document = response;

            },
            err => console.log(err),
        );


    }

    getAllRepertoire() {
        this.repertoireservice.getAllRepertoire(this.id_user).subscribe(
            response => {

                this.repertoire = response['repertoires'];
                // this.codeRepertoires=this.getListCodeRepertoire();
            },
            err => console.log(err),
        );

    }

    // getListCodeRepertoire(){
    //   for(let i=0;i<this.repertoire.length;i++){
    //     this.repertoire[i].codeRepertoire;
    //     console.log(this.repertoire[i].codeRepertoire);
    //     this.codeRepertoires.push(this.repertoire[i].codeRepertoire)
    //
    //
    //   }
    //
    //
    //   return  this.codeRepertoires;
    // }
}
