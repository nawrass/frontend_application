import {Component, Inject, OnInit} from '@angular/core';
import {Question} from "../entity/question";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {FaqService} from "../services/faq.service";
import {TokenStorageService} from "../auth/token-storage.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-edite-question',
  templateUrl: './edite-question.component.html',
  styleUrls: ['./edite-question.component.scss']
})
export class EditeQuestionComponent implements OnInit {

    question:Question=new Question();

    private roles: string[];
    private authority: string;
    id_user: number = this.tokenStorage.getId();

    oppoSuitsForm_supprimer = this.fb.group({
        name: ['']
    })
    myFiles: any = []
    studentupdateform=new FormGroup({

        question:new FormControl(),
        reponses:new FormControl()
    });
    constructor(private faqService :FaqService,private dialogRef: MatDialogRef<EditeQuestionComponent>,
                @Inject(MAT_DIALOG_DATA) private data: any,private tokenStorage: TokenStorageService,
                public dialog: MatDialog,public fb: FormBuilder) { }

    ngOnInit() {
        this.question=this.data.doc

        console.log("data"+JSON.stringify(this.data.doc))
        if (this.tokenStorage.getToken()) {
            this.roles = this.tokenStorage.getAuthorities();
            this.roles.every(role => {
                if (role === 'ROLE_USER') {
                    this.authority = 'user';
                    return true;
                } else if (role === 'ROLE_ADMIN') {
                    this.authority = 'admin';
                    return true;
                }
                this.authority = 'admin';
                return true;

            });
        }
        console.log("autority"+JSON.stringify(this.roles))
    }


  editeQuestion(){

                this.faqService.editeQuetion(this.question,this.question.id)
                    .subscribe(
                        (data :any)=> {
                            this.question=data
                            console.log(data);
                        },
                        (err) => {
                            console.warn("an error has occured", err)
                        }
                    );
      // this.question=new Question();
            }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
