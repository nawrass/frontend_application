import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditeQuestionComponent } from './edite-question.component';

describe('EditeQuestionComponent', () => {
  let component: EditeQuestionComponent;
  let fixture: ComponentFixture<EditeQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditeQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditeQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
