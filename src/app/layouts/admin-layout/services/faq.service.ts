import { Injectable } from '@angular/core';
import {TokenStorageService} from "../auth/token-storage.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {Question} from "../entity/question";
import {Repertoire} from "../entity/repertoire";

@Injectable({
  providedIn: 'root'
})
export class FaqService {
  id_user: number = this.token.getId();
  constructor(private token: TokenStorageService, private httpClient: HttpClient) { }

  URL_BASE = environment.apiBaseUrl;

  getAllQuestion(){
    const token = this.token.getToken();
      return this.httpClient.get<Question[]>("http://localhost:8089/api/question",{
      headers: {Authorization: `Bearer ${token}`},
      responseType: 'json'
    })
  }
    createQuestion(question: Question ,id_user:number){
        const token = this.token.getToken();
        return this.httpClient.post("http://localhost:8080/document/Object/post/"+this.id_user,question,{
            headers: {Authorization: `Bearer ${token}`},
            responseType: 'json'
        })
    }
  // createQuestion(question: Question ,id_user:number){
  //   const token = this.token.getToken();
  //     return this.httpClient.post("http://localhost:8080/document/Object/post/"+this.id_user,question,{
  //       headers: {Authorization: `Bearer ${token}`},
  //       responseType: 'json'
  //     })
  // }
  deleteQuestion(id:number){
      const token = this.token.getToken();
      return this.httpClient.delete("http://localhost:8080/document/Object/delete/"+id,{
          headers: {Authorization: `Bearer ${token}`},
          responseType: 'json'
      })
  }
    editeQuetion(question: Question ,id:number){
        const token = this.token.getToken();
        return this.httpClient.put("http://localhost:8080/document/Object/update/"+id,question,{
            headers: {Authorization: `Bearer ${token}`}
        })
    }
    searchQuestion(question:string){
        const token = this.token.getToken();
        return this.httpClient.get("http://localhost:8080/document/Object/search?contains="+question,{
            headers: {Authorization: `Bearer ${token}`},
            responseType: 'json'
        })
    }
}
