import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {User} from "../entity/user";
import {JwtHelperService} from "@auth0/angular-jwt";
import {TokenStorageService} from "../auth/token-storage.service";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    URL_BASE = 'http://localhost:8079/api/auth';
    URL_UPDT_USERNAME = '/updateUsername';
    URL_UPDT_PASSWORD = '/updatePassword';
    URL_UPDT_IMAGE = '/updateImageName';
    URL_GET_IMG = '/getUserImageName';
    URL_GET_USER = '/getUser';
    URL_CHECk_EMAIL = '/checkMail';
    routeToGo = '/';
    helper = new JwtHelperService();

    constructor(private http: HttpClient, private token: TokenStorageService) {
    }

    currentUser: any = null;

    setUserToGo(route: string) {
        this.routeToGo = route;
    }

    getRouteToGo() {
        return this.routeToGo;
    }

    validToken(): boolean {
        if (this.getAuthorizationToken() !== null) {
            if (!this.helper.isTokenExpired(this.getAuthorizationToken())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    decodeToken(token: any) {
        return this.helper.decodeToken(token);
    }

    updateUsername(email: String, username: String) {
        const addBody = {
            'email': email,
            'username': username
        };

        return this.http.post<User>(this.URL_BASE + this.URL_UPDT_USERNAME, addBody);
    }


    updateImage(email: String, imgName: String) {
        const addBody = {
            'email': email,
            'imageName': imgName
        };

        return this.http.post<User>(this.URL_BASE + this.URL_UPDT_IMAGE, addBody);
    }

    checkEmail(email: string) {
        let params = new HttpParams();

        params = params.append('email', email);
        return this.http.get<boolean>(this.URL_BASE + this.URL_CHECk_EMAIL, {params: params});
    }

    updatePassword(username: String, oldPass: String, newPass: String) {
        const addBody = {
            'username': username,
            'oldPass': oldPass,
            'newPass': newPass
        };

        return this.http.post<User>(this.URL_BASE + this.URL_UPDT_PASSWORD, addBody);
    }


    getUserImageName(username: string) {
        const token = this.token.getToken();
        let params = new HttpParams();
        params = params.append('username', username);
        return this.http.get<any>(this.URL_BASE + this.URL_GET_IMG, {
            params: params,
            headers: {Authorization: `Bearer ${token}`}
        });

    }

    getuserImageById(id: number) {
        const token = this.token.getToken();
        return this.http.get("http://localhost:8079/api/auth/getUserImageName/" + id, {
            headers: {Authorization: `Bearer ${token}`},
            responseType: "arraybuffer"
        })
    }

    getCurrentUser(email: string): any {
        return this.getUserAfterSignin(email);
    }

    setUserConnected(user: any) {
        this.currentUser = user;
    }

    getUserConnected() {
        return this.currentUser;
    }

    getAuthorizationToken() {
        return localStorage.getItem('access_token');
    }

    getUser(email: string) {
        let params = new HttpParams();
        params = params.append('email', email);
        return this.http.get<User>(this.URL_BASE + this.URL_GET_USER, {params: params});

    }


    getUserAfterSignin(email: string) {
        return this.getUser(email);
    }

    isUserLoggedIn() {
        let user = localStorage.getItem('username')
        console.log(!(user === null))
        return !(user === null)
    }

    logOut() {
        localStorage.removeItem('username')
    }

}
