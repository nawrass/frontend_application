import {TestBed} from '@angular/core/testing';

import {SearchdocumentService} from './searchdocument.service';

describe('SearchdocumentService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: SearchdocumentService = TestBed.get(SearchdocumentService);
        expect(service).toBeTruthy();
    });
});
