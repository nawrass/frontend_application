import {User} from "./user";

export class Repertoire {
    public id: any;
    public nomRepertoire: string;
    public dateCreation: Date;
    public codeRepertoire: string;
    public documents: Document[];
    public user: User[];


}
