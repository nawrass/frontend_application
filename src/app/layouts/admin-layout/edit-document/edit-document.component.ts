import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {DocumentService} from "../services/document.service";
import {Document} from "../entity/document";
import {RepertoireService} from "../services/repertoire.service";
import {Repertoire} from "../entity/repertoire";
import {NotificationService} from "../services/notification.service";
import {TokenStorageService} from "../auth/token-storage.service";

@Component({
    selector: 'app-edit-document',
    templateUrl: './edit-document.component.html',
    styleUrls: ['./edit-document.component.scss']
})

export class EditDocumentComponent implements OnInit {

    constructor(private token: TokenStorageService, public dialog: MatDialog,
                public repertoireservice: RepertoireService,
                private documentservice: DocumentService,
                @Inject(MAT_DIALOG_DATA) private data: any,
                public notificationService: NotificationService,
                private dialogRef: MatDialogRef<EditDocumentComponent>) {
    }

    document: Document[];
    repertoire: Repertoire;
    // repertoires: Repertoire;
    extension: any;
    documents: Document = new Document();
    newDoc: string;
    id_user: number = this.token.getId();

    ngOnInit() {
        this.getAllDocument();
        this.documents = this.data.doc;
        console.log("myDoc" + this.documents)
        this.newDoc = this.documents.nomDoc.split('.').slice(0, -1).join('.')
        console.log("newdoc " + this.newDoc)
        this.extension = this.documents.nomDoc.substr((this.documents.nomDoc.lastIndexOf('.') + 1))
        console.log("extension " + this.extension)
        console.log("nouveaudoc ," + this.newDoc + "." + this.extension)


    }

    editDocument() {

        this.documentservice.editDocument(this.documents.id, this.newDoc + "." + this.extension).subscribe((val: any) => {
                this.documents.nomDoc = val;
                console.log("val+ ", val);
                // this.notificationService.success(':: Submitted successfully')


            },
            (err) => {
                console.warn("an error has occured", err)
            },

            () => {
                this.getAllRepertoire();
            }
        );

    }

    getAllRepertoire() {
        this.repertoireservice.getAllRepertoire(this.id_user).subscribe(
            response => {

                this.repertoire = response['repertoires'];

            },
            err => console.log(err),
        );
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    getAllDocument() {
        return this.documentservice.getAllDocument(this.id_user).subscribe(
            (data: any) => {

                this.document = data;
                console.log(" this.document+", this.document)
                console.log('data: ', data)
                //
            },
            err => console.log(err),
        );
    }
}
