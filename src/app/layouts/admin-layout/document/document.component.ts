import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import {DocumentService} from "../services/document.service";
import {Document} from 'app/layouts/admin-layout/entity/document';
import {RepertoireService} from "../services/repertoire.service";
import {Repertoire} from "../entity/repertoire";
import {ActivatedRoute, Router} from "@angular/router";
import {FormArray, FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {HttpClient, HttpErrorResponse, HttpEventType, HttpParams, HttpResponse} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {RepertoireComponent} from "./repertoire/repertoire.component";
import {ReplacedocumentComponent} from "../replacedocument/replacedocument.component";
import {GlobalConstant} from "../../../common/GlobalConstant";
import {EditDocumentComponent} from "../edit-document/edit-document.component";
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {PageService} from "../services/page.service";
import {Sort} from "@angular/material/sort";
import {PageEvent} from "@angular/material/paginator";
import {fromMatPaginator, paginateRows} from "./datasource-util";
import {fromMatSort, sortRows} from './datasource-util';
import {debounceTime, distinctUntilChanged, map, switchMap, tap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {GridOptions} from "ag-grid-community";
import {DropDownEditorComponent} from "../drop-down-editor/drop-down-editor.component";
import {DetailDocumentComponent} from "../detail-document/detail-document.component";
import {content} from "googleapis/build/src/apis/content";
import * as fileSaver from 'file-saver';
import {DeletDocumentComponent} from "../delet-document/delet-document.component";
import {User} from "../entity/user";
import {TokenStorageService} from "../auth/token-storage.service";
import * as _ from "lodash";

@Component({
    selector: 'app-document',
    templateUrl: './document.component.html',
    styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {
    // ngAfterViewInit(): void {
    //     this.activatedRoute.paramMap.subscribe((res)=>{
    //
    //         this.id = res["params"]["id"]
    //     })
    // }
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    totalRows$: Observable<number>;
    @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;
    files = [];
    panelOpenState = false;
    document: any;
    oppoSuitsForm = this.fb.group({
        name: ['']
    });
    oppoSuitsForm_delete = this.fb.group({
        name: ['']
    });
    public gridOptions: GridOptions;
    user: User;
    repertoire: any;
    repertoire_deplacer: Repertoire[];
    documents: any
    // doc:any;
    repertoires: Repertoire = new Repertoire();
    progress: { percentage: number } = {percentage: 0};
    selectedFile = null;
    codeRepertoires: Array<String> = [];
    nomDocuments: Array<String> = [];
    codeRepertoires_deplacer: Array<String> = [];
    pager: any = {};
    optionSelect: any = [];
    optionSelectNomDoc: any = [];
    filerows: any = [];
    selectedOptions: string[] = [];
    oppoSuits_ajout: Array<String> = [];
    file: FileList;
    codeRepertoireTable: any = [];
    form: FormGroup;
    xpandStatus = false;
    id_user: number = this.token.getId();
    extension: any;
    iconList = [ // array of icon class list based on type
        {type: "xlsx", icon: "fa fa-file-excel-o"},
        {type: "pdf", icon: "fa fa-file-pdf-o", color: "#00FF00"},
        {type: "jpg", icon: "fa fa-file-image-o"},
        {type: "JPG", icon: "fa fa-file-image-o"},
        {type: "PNG", icon: "fa fa-file-image-o"},
        {type: "png", icon: "fa fa-file-image-o"}
    ];

    constructor(private token: TokenStorageService,
                private documentservice: DocumentService,
                private repertoireservice: RepertoireService,
                private router: Router,
                public fb: FormBuilder,
                public dialog: MatDialog,
                public activatedRoute: ActivatedRoute, private pagerService: PageService,
                private http: HttpClient
    ) {

        this.gridOptions = <GridOptions>{
            // enableSorting: true,
            // enable filtering
            // enableFilter: true,
            editable: true
        };
        this.gridOptions.columnDefs = [
            {
                headerName: "Document",
                field: "nomDoc",
                width: 250
            },
            {
                headerName: "Repertoire",
                field: "CodeRepSelect",

                cellEditorFramework: DropDownEditorComponent,
                editable: true,
                cellEditorParams: {
                    options: this.optionSelect
                }
            },

        ];


    }

    myFiles: any = []
    selectedHobbiesNames: [string];
    FilesForm: FormGroup;

    createHobbies(hobbiesInputs) {
        const arr = hobbiesInputs.map(hobby => {
            return new FormControl(hobby.selected || false);
        });
        return new FormArray(arr);
    }

    createFormInputs() {
        this.FilesForm = new FormGroup({
            hobbies: this.createHobbies(this.myFiles)
        });
        this.getSelectedHobbies();
    }

    getSelectedHobbies() {
        this.selectedHobbiesNames = _.map(
            this.FilesForm.controls.hobbies["controls"],
            (hobby, i) => {
                return hobby.value && this.myFiles[i].value;
            }
        );
        this.getSelectedHobbiesName();
    }

    getSelectedHobbiesName() {
        this.selectedHobbiesNames = _.filter(
            this.selectedHobbiesNames,
            function (hobby) {
                if (hobby !== false) {
                    return hobby;
                }
            }
        );
    }

    displayedColumns = ['nomRepertoire'];
    id: number;
    rowData: any;
    oppoSuitsForm_deplacer = this.fb.group({
        name: ['']
    })

    selectedFiles = this.fb.group({
        profile: ['']
    });
    formDatas: any[];

    ngOnInit() {

        this.getAllRepertoire();
        console.log("repertoire1," + this.getAllRepertoire());
        console.log("liste" + this.getListNomDoc());
        this.getPaginatorData(event);
        this.getListCodeRepertoire();

        this.oppoSuits_ajout = this.getListCodeRepertoire();
        this.createFormInputs();


    }

    getFileExtension(doc) {

        let ext = doc.split(".").pop();
        console.log("ext" + ext)
        let obj = this.iconList.filter(row => {
            if (row.type === ext) {
                return true;
            }
        });
        if (obj.length > 0) {
            let icon = obj[0].icon;
            return icon;
        } else {
            return "";
        }
    }

    listRepertoireToSelectOption() {
        for (let i = 0; i < this.repertoire.length; i++) {
            const repertoireObject = {
                name: this.repertoire[i].codeRepertoire,
                value: this.repertoire[i].nomRepertoire
            }
            this.optionSelect.push(repertoireObject)
            console.log("optionSelect" + this.optionSelect.push(repertoireObject))
        }
    }

    pageIndex: number = 0;
    pageSize: number = 5;
    lowValue: number = 0;
    highValue: number = 10;

    getPaginatorData(event) {
        console.log(event);
        if (event.pageIndex === this.pageIndex + 1) {
            this.lowValue = this.lowValue + this.pageSize;
            this.highValue = this.highValue + this.pageSize;
        } else if (event.pageIndex === this.pageIndex - 1) {
            this.lowValue = this.lowValue - this.pageSize;
            this.highValue = this.highValue - this.pageSize;

        }
        this.pageIndex = event.pageIndex;
    }

    getListCodeRepertoire() {
        for (let i = 0; i < this.repertoire.length; i++) {
            this.repertoire[i].codeRepertoire;
            console.log(this.repertoire[i].codeRepertoire);
            this.codeRepertoires.push(this.repertoire[i].codeRepertoire)
            const repertoireObject = {
                name: this.repertoire[i].codeRepertoire,
                value: this.repertoire[i].nomRepertoire
            }
            this.optionSelect.push(repertoireObject)

        }
        console.log("optionSelectfromgetlist" + this.optionSelect[2].value)

        console.log(this.codeRepertoires);
        return this.codeRepertoires;
    }

    getListNomDoc() {
        console.log("teste2")
        for (let i = 0; i < this.document.length; i++) {
            this.document[i].nomDoc;
            console.log("test1" + this.document[i].nomDoc);
            this.nomDocuments.push(this.document[i].nomDoc)


        }

    }

    onSubmit() {
        alert(JSON.stringify(this.oppoSuitsForm.value))
        console.log(this.codeRepertoires);
    }

    multiFiles() {

        for (var val of this.filerows) {
            console.log('files,' + this.filerows)
            let formData = new FormData();
            formData.append('file', val.file);
            formData.append('codeRepertoire', val.CodeRepSelect);
            console.log("coderep:" + val.CodeRepSelect)
            console.log(formData.get('file'))

            this.documentservice.uploadFile(formData, this.id_user).subscribe((val) => {
                    this.selectedFiles = val.toString();
                    this.reset();
                    this.filerows = [''];
                },
                (err) => {
                    console.warn("an error has occured", err)
                },

                () => {
                    this.getAllRepertoire();
                }
            );

        }

    }
    reset(){
        this.oppoSuitsForm.reset()
    }
    rests(){
        this.selectedFiles.reset();
    }
    nomRepertoireSpecifique: string;

    onChangeRepertoireSpecifique(value: string, nomFile: string) {
        this.nomRepertoireSpecifique = value;
        console.log("this.nomRepertoireSpecifique" + this.nomRepertoireSpecifique)

        if (this.nomRepertoireSpecifique == null) {
            this.nomRepertoireSpecifique = this.oppoSuitsForm.value.name;

        }


        for (let data of this.filerows) {

            if (data.file.name == nomFile) {

                data.CodeRepSelect = this.nomRepertoireSpecifique;

            }
        }
        this.nomRepertoireSpecifique= '';
    }

    onChangeValue() {

        this.nomRepertoireSpecifique = this.oppoSuitsForm_delete.value.name;


    }

    selectFile(event) {
        this.filerows = []
        const file = event.target.files;
        console.log("event", event.target.files);
        this.selectedFiles.get('profile').setValue(file);
        for (var val of this.selectedFiles.get('profile').value) {
            const fileObject = {
                nomDoc: val.name,
                file: val,
                CodeRepSelect: this.oppoSuitsForm.value.name

            }
            this.filerows.push(fileObject);

        }
        console.log("filerows" + this.filerows.valueOf())

        this.rowData = this.filerows;

        event.target.value = '';
    }

    nomDoc: string;
    nomRepertoire: string;
    datasource = new MatTableDataSource(this.repertoire);

    searchNomDoc() {
        console.log("getnom:" + this.nomDoc)
        if (this.nomDoc == "") {
            this.repertoireservice.getAllRepertoire(this.id_user).subscribe(
                response => {

                    this.repertoire = response['repertoires'];
                    this.codeRepertoires = this.getListCodeRepertoire();
                },
                err => console.log(err),
            );
        }
        this.documentservice.searchHeroes(this.nomDoc, this.id_user).subscribe(
            (response: any) => {

                this.repertoire = response;
                // console.log("doc"+this.document)
            },
            err => console.log(err),
        );
    }

    save(data) {
        console.log("repertoire", data);

        this.repertoireservice.postRepertoire(data.nomRepertoire, this.id_user)
            .subscribe(
                (data: any) => {


                    console.log("success")
                    this.repertoires.nomRepertoire = ' ';

                },
                (err) => {
                    console.warn("an error has occured", err)
                },
                () => {
                    this.getAllRepertoire();
                });

    }

    getAllRepertoire() {
        console.log("iduser : " + this.id_user)

        this.repertoireservice.getAllRepertoire(this.id_user).subscribe(
            response => {

                this.repertoire = response['repertoires'];
                console.log("rep1" + JSON.stringify(this.repertoire))
                this.getIdDoc();
                console.log("myliste" + JSON.stringify(this.myFiles))
                // this.codeRepertoires=this.getListCodeRepertoire();
            },
            err => console.log(err),
        );

    }

    getIdDoc() {
        this.myFiles = [];
        for (let i = 0; i < this.repertoire.length; i++) {
            for (let j = 0; j < this.repertoire[i].documents.length; j++) {
                const fileObject = {
                    id: this.repertoire[i].documents[j].id,
                    nomDoc: this.repertoire[i].documents[j].nomDoc,
                    type: this.repertoire[i].documents[j].type,
                    selected: false
                }
                this.myFiles.push(fileObject)
            }
        }
    }

    checkDoc(event, id: number) {
        for (let i = 0; i < this.myFiles.length; i++) {
            console.log("select1" + this.myFiles.length);
            if (id == this.myFiles[i].id) {
                this.myFiles[i].selected = event.checked;
                console.log("select" + JSON.stringify(this.myFiles[i]));
                break
            }
        }

    }

    telechargerAll() {
        for (let i = 0; i < this.myFiles.length; i++) {
            if (this.myFiles[i].selected == true) {
                console.log("myfiles" + JSON.stringify(this.myFiles[i]))
                this.documentservice.downloadFile(this.myFiles[i].id).pipe(tap(byteArray => {
                    // @ts-ignore
                    let newBlob = new Blob([byteArray], {type: this.myFiles[i].type});
                    fileSaver.saveAs(newBlob, this.myFiles[i].nomDoc);
                })).subscribe();
            }
        }
    }

    supprimerAllFile() {
        for (let i = 0; i < this.myFiles.length; i++) {
            if (this.myFiles[i].selected == true) {
                console.log("myfiles" + JSON.stringify(this.myFiles[i]))
                this.documentservice.deleteDocument(this.myFiles[i].id)
                    .subscribe(
                        data => {
                            console.log(data);
                        },
                        (err) => {
                            console.warn("an error has occured", err)
                        },
                        () => {
                            this.getAllRepertoire();
                        }
                    );
            }
        }
    }

    getAllDocument() {
        this.documentservice.getAllDocument(this.id_user).subscribe(
            (response: any) => {

                this.document = response;

            },
            err => console.log(err),
        );

    }

    deletContentDirectory(codeRep: string) {

        this.documentservice.deletContentDirectory(codeRep)
            .subscribe(
                data => {
                    console.log(data);
                },
                (err) => {
                    console.warn("an error has occured", err)
                },
                () => {
                    this.getAllRepertoire();
                }
            );
    }

    openAlertDialog(id: number) {
        console.log("dialogid" + id)
        let formData = new FormData();
        console.log('codeRepertoire', this.oppoSuitsForm_deplacer.value.name);
        formData.append('codeRepertoire', this.oppoSuitsForm_deplacer.value.name);


        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        const dialogRef = this.dialog.open(ReplacedocumentComponent, {
            data: {
                id: id

            }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.getAllRepertoire();
        });

    }

    deleteDocument(doc: any) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        const dialogRef = this.dialog.open(DeletDocumentComponent, {
                data: {
                    doc: doc
                }
            }
        );
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.getAllRepertoire();
        });
    }

    editDocument(doc: any) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "60%";
        const dialogRef = this.dialog.open(EditDocumentComponent, {
                data: {
                    doc: doc
                }
            }
        );
        dialogRef.afterClosed().subscribe(result => {
            this.getAllRepertoire();
        });
    }

    detailDocument(doc: any) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = "50%";
        dialogConfig.height = "20%";
        const dialogRef = this.dialog.open(DetailDocumentComponent, {
                data: {
                    doc: doc
                }
            }
        );
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.getAllRepertoire();
        });
    }

    downloadFile(doc: Document) {
        this.documentservice.downloadFile(doc.id).pipe(tap(byteArray => {
            // @ts-ignore
            let newBlob = new Blob([byteArray], {type: doc.type});
            fileSaver.saveAs(newBlob, doc.nomDoc);
        })).subscribe();
    }


    @Output()
    searchListen = new EventEmitter<String>();
    searchText: any;

    changeit() {
        this.searchListen.emit(this.searchText);
    }
}


