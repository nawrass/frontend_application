import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

import {JwtResponse} from './jwt-response';
import {AuthLoginInfo} from './login-info';
import {SignUpInfo} from './signup-info';
import {User} from "../entity/user";

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private loginUrl = 'http://localhost:8079/api/auth/signin';
    private signupUrl = 'http://localhost:8079/api/auth/signup';
    private roles:Array<any>=[];
    constructor(private http: HttpClient) {
    }

    getAuthorizationToken() {
        return localStorage.getItem('access_token');
    }

    attemptAuth(credentials: AuthLoginInfo): Observable<JwtResponse> {

        return this.http.post<JwtResponse>(this.loginUrl, credentials);
    }


    signup(user: User): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');

        return <Observable<any>>this.http.post(this.signupUrl, user);
    }


}
