import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-document-search',
    templateUrl: './document-search.component.html',
    styleUrls: ['./document-search.component.scss']
})
export class DocumentSearchComponent implements OnInit {

    @Output()
    searchListen = new EventEmitter<String>();
    searchText: any;

    constructor() {
    }

    ngOnInit() {
    }


    changeit() {
        this.searchListen.emit(this.searchText);
    }

}
