import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DeletDocumentComponent} from './delet-document.component';

describe('DeletDocumentComponent', () => {
    let component: DeletDocumentComponent;
    let fixture: ComponentFixture<DeletDocumentComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DeletDocumentComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DeletDocumentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
