import {Component, Inject, OnInit} from '@angular/core';
import {DocumentService} from "../services/document.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Document} from "../entity/document";
import {Repertoire} from "../entity/repertoire";
import {RepertoireService} from "../services/repertoire.service";
import {TokenStorageService} from "../auth/token-storage.service";

@Component({
    selector: 'app-delet-document',
    templateUrl: './delet-document.component.html',
    styleUrls: ['./delet-document.component.scss']
})
export class DeletDocumentComponent implements OnInit {

    constructor(private token: TokenStorageService, private documentservice: DocumentService, private repertoireservice: RepertoireService,
                @Inject(MAT_DIALOG_DATA) private data: any,
                private dialogRef: MatDialogRef<DeletDocumentComponent>) {
    }

    documents: Document = new Document();
    repertoire: Repertoire[];
    id_user: number = this.token.getId();

    ngOnInit() {
        this.documents = this.data.doc;

    }

    deleteDocument() {

        this.documentservice.deleteDocument(this.documents.id)
            .subscribe(
                data => {
                    console.log(data);
                },
                (err) => {
                    console.warn("an error has occured", err)
                },
                () => {
                    this.getAllRepertoire();
                }
            );
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    getAllRepertoire() {
        this.repertoireservice.getAllRepertoire(this.id_user).subscribe(
            response => {

                this.repertoire = response['repertoires'];
            },
            err => console.log(err),
        );

    }
}
