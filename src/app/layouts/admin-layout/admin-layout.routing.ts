import {Routes} from '@angular/router';

import {DashboardComponent} from '../../dashboard/dashboard.component';
import {TableListComponent} from '../../table-list/table-list.component';
import {TypographyComponent} from '../../typography/typography.component';
import {IconsComponent} from '../../icons/icons.component';
import {DocumentComponent} from 'app/layouts/admin-layout/document/document.component';
import {RepertoireComponent} from "app/layouts/admin-layout/document/repertoire/repertoire.component";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {ProfileComponent} from "./profile/profile.component";
import {FaqComponent} from "./faq/faq.component";

export const AdminLayoutRoutes: Routes = [
    {path: 'dashboard', component: DashboardComponent},
    {path: 'table-list', component: TableListComponent},
    {path: 'typography', component: TypographyComponent},
    {path: 'icons', component: IconsComponent},
    {path: 'document', component: DocumentComponent},
    {path: 'repertoire', component: RepertoireComponent},
    {path: 'profile', component: ProfileComponent},
    {
        path: 'register', component: RegisterComponent
    },
    {path: 'faq', component: FaqComponent},

];
