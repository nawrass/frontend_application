import {Component, Inject, OnInit} from '@angular/core';
import {TokenStorageService} from "../auth/token-storage.service";
import {DocumentService} from "../services/document.service";
import {RepertoireService} from "../services/repertoire.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Question} from "../entity/question";
import {FaqService} from "../services/faq.service";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-delete-question',
  templateUrl: './delete-question.component.html',
  styleUrls: ['./delete-question.component.scss']
})
export class DeleteQuestionComponent implements OnInit {

  constructor(private faqService :FaqService,public fb: FormBuilder,private token: TokenStorageService, private documentservice: DocumentService, private repertoireservice: RepertoireService,
              @Inject(MAT_DIALOG_DATA) private data: Question,
              private dialogRef: MatDialogRef<DeleteQuestionComponent>) { }
question :Question=new Question();
  questions:Question[];
    listQuestion: Array<String> = [];
    oppoSuitsForm_supprimer = this.fb.group({
        name: ['']
    })
  ngOnInit() {
    this.question=this.data.id;
  }
  deleteQuestion() {
      let formData = new FormData();
      formData.append('question', this.oppoSuitsForm_supprimer.value.name);
    this.faqService.deleteQuestion(this.data.id)
        .subscribe(
            data => {
              console.log(data);
            },
            (err) => {
              console.warn("an error has occured", err)
            },
            () => {
              this.getAllQuestion();
            }
        );
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  getAllQuestion(){

    return this.faqService.getAllQuestion().subscribe(
        response => {
          this.questions = response;
          console.log("liste"+this.questions)

        },
        err => console.log(err)
    );
  }
    getListCodeRepertoire() {
        for (let i = 0; i < this.questions.length; i++) {
            this.questions[i].question;
            console.log(this.questions[i].question);
            this.listQuestion.push(this.questions[i].question)

        }
        console.log(this.listQuestion);
        return this.listQuestion;
    }
}
