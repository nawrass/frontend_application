import {Component, Inject, OnInit} from '@angular/core';
import {DocumentService} from "../services/document.service";
import {RepertoireService} from "../services/repertoire.service";
import {Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from "@angular/material/dialog";
import {DocumentComponent} from "../document/document.component";
import {Repertoire} from "../entity/repertoire";
import {Document} from "../entity/document";
import {Subject} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {NotificationService} from "../services/notification.service";
import {TokenStorageService} from "../auth/token-storage.service";

@Component({
    selector: 'app-replacedocument',
    templateUrl: './replacedocument.component.html',
    styleUrls: ['./replacedocument.component.scss']
})
export class ReplacedocumentComponent implements OnInit {
    document: Document[];
    showSuccess = false;
    repertoire: Repertoire[];
    repertoire_deplacer: Repertoire[];
    documents: any
    doc: any;
    id: number
    repertoires: Repertoire = new Repertoire();
    private refreshmethode = new Subject<void>();
    currentFileUpload: File;
    progress: { percentage: number } = {percentage: 0};
    selectedFile = null;
    codeRepertoires: Array<String> = [];
    codeRepertoires_deplacer: Array<String> = [];

    oppoSuits_ajout: Array<String> = [];
    oppoSuits_deplacer: Array<String> = [];


    ngOnInit() {
        console.log("data1" + this.data.id)
        console.log('codeRepertoire', this.oppoSuitsForm_deplacer.value.name)

        this.getAllRepertoire();
        this.getListCodeRepertoire();
        this.oppoSuits_deplacer = this.getListCodeRepertoire();

    }


    constructor(private token: TokenStorageService, private repertoireservice: RepertoireService,
                private documentservice: DocumentService,
                public dialog: MatDialog,
                private _snackBar: MatSnackBar,
                public fb: FormBuilder,
                private notificationService: NotificationService,
                @Inject(MAT_DIALOG_DATA) private data: Document,
                private dialogRef: MatDialogRef<ReplacedocumentComponent>) {

    }

    id_user: number = this.token.getId();
    oppoSuitsForm = this.fb.group({
        name: ['']
    })
    oppoSuitsForm_deplacer = this.fb.group({
        name: ['']
    })


    enregistreDocument() {
        console.log("data" + this.data.id)

        let formData = new FormData();
        formData.append('codeRepertoire', this.oppoSuitsForm_deplacer.value.name);
        console.log('codeRepertoire', this.oppoSuitsForm_deplacer.value.name)
        const docum = this.documentservice.replaceDocument(this.data.id, formData).subscribe((val) => {

                console.log(val);
              this.notificationService.success(':: Submitted successfully')


            },
            (err) => {
                console.warn("an error has occured", err)
            },
            () => {
                this.getAllRepertoire();
            }
        );

    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        alert(JSON.stringify(this.oppoSuitsForm.value))
        console.log(this.codeRepertoires);
    }

    getListCodeRepertoire() {
        for (let i = 0; i < this.repertoire.length; i++) {
            this.repertoire[i].codeRepertoire;
            console.log(this.repertoire[i].codeRepertoire);
            this.codeRepertoires.push(this.repertoire[i].codeRepertoire)

        }
        console.log(this.codeRepertoires);
        return this.codeRepertoires;
    }

    getAllRepertoire() {
        this.repertoireservice.getAllRepertoire(this.id_user).subscribe(
            response => {

                this.repertoire = response['repertoires'];

                this.codeRepertoires = this.getListCodeRepertoire();
            },
            err => console.log(err),
            () => {

            }
        );
        console.log("test2" + this.repertoire);
    }
}
