import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as Chartist from 'chartist';
import {User} from "../layouts/admin-layout/entity/user";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../layouts/admin-layout/services/user.service";
import {AuthService} from "../layouts/admin-layout/auth/auth.service";
import {AuthLoginInfo} from "../layouts/admin-layout/auth/login-info";
import {TokenStorageService} from "../layouts/admin-layout/auth/token-storage.service";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    form: any = {};
    isLoggedIn = false;
    isLoginFailed = false;
    errorMessage = '';
    roles: String [];
    id: any;
    private loginInfo: AuthLoginInfo;
    showSpinner = false;
    @Input()
    modal: any;
    userNotFound = false;

    constructor(private tokenStorage: TokenStorageService, private userService: UserService, private auth: AuthService, private router: Router) {
    }

    username = '';
    email = '';
    @Output()
    signupOpen = new EventEmitter<boolean>();

    ngOnInit() {
        if (this.tokenStorage.getToken()) {
            this.isLoggedIn = true;
            this.roles = this.tokenStorage.getAuthorities();

        }
    }

    onSubmit() {
        console.log(this.form);

        this.loginInfo = new AuthLoginInfo(
            this.form.username,
            this.form.password);

        this.auth.attemptAuth(this.loginInfo).subscribe(
            data => {
                console.log(data)
                this.tokenStorage.saveToken(data.accessToken);
                this.tokenStorage.saveUsername(data.username);
                console.log(data.username);
                this.tokenStorage.saveId(data.id);

                this.id = this.tokenStorage.saveId(data.id);
                console.log("dataemail" + this.tokenStorage.getEmail());
                console.log("data" + data.id);

                console.log("ddata.authorities : "+JSON.stringify(data.authorities));
                this.tokenStorage.saveAuthorities(data.authorities);

                this.isLoginFailed = false;
                this.isLoggedIn = true;
                this.roles = this.tokenStorage.getAuthorities();
                console.log("datarole" + data.authorities);
                this.router.navigate(['/document']);
            },

            error => {
                console.log(error);
                this.errorMessage = error.error.message;
                this.isLoginFailed = true;
                this.showSpinner = true;
            }
        );
    }

    reloadPage() {
        window.location.reload();
    }

    recuperationId(): any {

        return this.id;

    }


    onClose() {
        this.modal.hide();
    }

    openSignup() {
        // this.signupOpen.emit(true);
        this.router.navigate(['/register']);
    }


    passFocus() {
        this.userNotFound = false;
    }


}


//
//   constructor() { }
//   startAnimationForLineChart(chart){
//       let seq: any, delays: any, durations: any;
//       seq = 0;
//       delays = 80;
//       durations = 500;
//
//       chart.on('draw', function(data) {
//         if(data.type === 'line' || data.type === 'area') {
//           data.element.animate({
//             d: {
//               begin: 600,
//               dur: 700,
//               from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
//               to: data.path.clone().stringify(),
//               easing: Chartist.Svg.Easing.easeOutQuint
//             }
//           });
//         } else if(data.type === 'point') {
//               seq++;
//               data.element.animate({
//                 opacity: {
//                   begin: seq * delays,
//                   dur: durations,
//                   from: 0,
//                   to: 1,
//                   easing: 'ease'
//                 }
//               });
//           }
//       });
//
//       seq = 0;
//   };
//   startAnimationForBarChart(chart){
//       let seq2: any, delays2: any, durations2: any;
//
//       seq2 = 0;
//       delays2 = 80;
//       durations2 = 500;
//       chart.on('draw', function(data) {
//         if(data.type === 'bar'){
//             seq2++;
//             data.element.animate({
//               opacity: {
//                 begin: seq2 * delays2,
//                 dur: durations2,
//                 from: 0,
//                 to: 1,
//                 easing: 'ease'
//               }
//             });
//         }
//       });
//
//       seq2 = 0;
//   };
//   ngOnInit() {
//       /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */
//
//       const dataDailySalesChart: any = {
//           labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
//           series: [
//               [12, 17, 7, 17, 23, 18, 38]
//           ]
//       };
//
//      const optionsDailySalesChart: any = {
//           lineSmooth: Chartist.Interpolation.cardinal({
//               tension: 0
//           }),
//           low: 0,
//           high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
//           chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
//       }
//
//       var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);
//
//       this.startAnimationForLineChart(dailySalesChart);
//
//
//       /* ----------==========     Completed Tasks Chart initialization    ==========---------- */
//
//       const dataCompletedTasksChart: any = {
//           labels: ['12p', '3p', '6p', '9p', '12p', '3a', '6a', '9a'],
//           series: [
//               [230, 750, 450, 300, 280, 240, 200, 190]
//           ]
//       };
//
//      const optionsCompletedTasksChart: any = {
//           lineSmooth: Chartist.Interpolation.cardinal({
//               tension: 0
//           }),
//           low: 0,
//           high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
//           chartPadding: { top: 0, right: 0, bottom: 0, left: 0}
//       }
//
//       var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);
//
//       // start animation for the Completed Tasks Chart - Line Chart
//       this.startAnimationForLineChart(completedTasksChart);
//
//
//
//       /* ----------==========     Emails Subscription Chart initialization    ==========---------- */
//
//       var datawebsiteViewsChart = {
//         labels: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
//         series: [
//           [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]
//
//         ]
//       };
//       var optionswebsiteViewsChart = {
//           axisX: {
//               showGrid: false
//           },
//           low: 0,
//           high: 1000,
//           chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
//       };
//       var responsiveOptions: any[] = [
//         ['screen and (max-width: 640px)', {
//           seriesBarDistance: 5,
//           axisX: {
//             labelInterpolationFnc: function (value) {
//               return value[0];
//             }
//           }
//         }]
//       ];
//       var websiteViewsChart = new Chartist.Bar('#websiteViewsChart', datawebsiteViewsChart, optionswebsiteViewsChart, responsiveOptions);
//
//       //start animation for the Emails Subscription Chart
//       this.startAnimationForBarChart(websiteViewsChart);
//   }
//
// }
