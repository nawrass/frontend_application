import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from "../layouts/admin-layout/services/user.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../layouts/admin-layout/auth/auth.service";
import {User} from "../layouts/admin-layout/entity/user";

@Component({
    selector: 'app-navba',
    templateUrl: './navba.component.html',
    styleUrls: ['./navba.component.scss']
})
export class NavbaComponent implements OnInit {
    imageRes = '';

    @Input()
    component: string;
    username = '';
    email = '';
    validToken = true;
    URL_BASE = 'http://localhost:8080';

    constructor(private userService: UserService, private route: Router) {
    }

    ngOnInit() {
        this.getValidity();
        this.getImageNameLocal();
    }

    clearCach() {
        localStorage.clear();
        this.getValidity();
        this.userService.setUserToGo('/welcome');

    }

    getValidity() {
        this.username = localStorage.getItem('username');
        this.email = localStorage.getItem('email');

        this.validToken = this.userService.validToken();
    }

    getImageNameLocal() {
        this.imageRes = this.URL_BASE + '/files/' + localStorage.getItem('imageName');
    }


}
