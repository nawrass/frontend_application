import {Component, OnInit, ElementRef, Input} from '@angular/core';
import {ROUTES} from '../sidebar/sidebar.component';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {Router} from '@angular/router';
import {UserService} from "../../layouts/admin-layout/services/user.service";
import {UploadService} from "../../layouts/admin-layout/services/upload.service";
import {TokenStorageService} from "../../layouts/admin-layout/auth/token-storage.service";
import {HttpClient} from "@angular/common/http";
import {tap} from "rxjs/operators";
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    email = '';
    imageRes: any;

    URL_BASE = 'http://localhost:8080';
    info: any;
    name = '';
    private listTitles: any[];
    location: Location;
    mobile_menu_visible: any = 0;
    private toggleButton: any;
    private sidebarVisible: boolean;
    id_user: number = this.token.getId();

    constructor(private httpClient: HttpClient, location: Location, private element: ElementRef, private token: TokenStorageService, private router: Router, private userService: UserService
        , private sanitizer: DomSanitizer) {
        this.location = location;
        this.sidebarVisible = false;

    }

    public html = '<span class="btn btn-danger">Your HTML here</span>';


    ngOnInit() {

        this.name = this.token.getUsername();
        this.email = this.token.getEmail();
        console.log("name" + this.name)
        // this.getImageNameLocal() ;
        this.getImageUserById();

        this.listTitles = ROUTES.filter(listTitle => listTitle);
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        this.router.events.subscribe((event) => {
            this.sidebarClose();
            var $layer: any = document.getElementsByClassName('close-layer')[0];
            if ($layer) {
                $layer.remove();
                this.mobile_menu_visible = 0;
            }
        });

    }

    logout() {
        this.token.signOut();
        this.router.navigate(['/dashboard']);
    }

    // getImageName(username: string) {
    //     this.userService.getUserImageName(username).subscribe(
    //         res => {this.imageRes = this.URL_BASE + '/files/' + res.imgn; console.log("img"+this.imageRes);
    //             localStorage.setItem('imageName', res.imgn);
    //
    //         } ,
    //         err => { console.log(err); },
    //         () => {
    //         }
    //     );
    // }
    getImageUserById() {

        this.userService.getuserImageById(this.id_user).pipe(tap(byteArray => {
            // @ts-ignore
            let newBlob = new Blob([byteArray]);
            this.imageRes = window.URL.createObjectURL(newBlob);
            document.querySelector("img").src = this.imageRes;
        })).subscribe(
            // res=>{
            //     this.imageRes = res;
            //     // this.imageRes = this.sanitizer.bypassSecurityTrustUrl(objectURL);
            //
            //
            //
            // }
        )

    }

    // getImage() {
    //     //Make a call to Sprinf Boot to get the Image Bytes.
    //     const token = this.token.getToken();
    //     this.httpClient.get('http://localhost:8080/files/' + this.imageRes,{headers: { Authorization: `Bearer ${token}` }})
    //         .subscribe(
    //             res => {
    //                 this.retrieveResonse = res;
    //                 this.base64Data = this.retrieveResonse.picByte;
    //                 this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
    //             }
    //
    //         );
    // }
    //
    getImageNameLocal() {
        const token = this.token.getToken();
        this.imageRes = this.URL_BASE + '/files/' + localStorage.getItem('imageName'), {
            headers: {Authorization: `Bearer ${token}`},
            responseType: 'blob'
        };
    }

    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);

        body.classList.add('nav-open');

        this.sidebarVisible = true;
    };

    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };

    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        var $toggle = document.getElementsByClassName('navbar-toggler')[0];

        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
        const body = document.getElementsByTagName('body')[0];

        if (this.mobile_menu_visible == 1) {
            // $('html').removeClass('nav-open');
            body.classList.remove('nav-open');
            if ($layer) {
                $layer.remove();
            }
            setTimeout(function () {
                $toggle.classList.remove('toggled');
            }, 400);

            this.mobile_menu_visible = 0;
        } else {
            setTimeout(function () {
                $toggle.classList.add('toggled');
            }, 430);

            var $layer = document.createElement('div');
            $layer.setAttribute('class', 'close-layer');


            if (body.querySelectorAll('.main-panel')) {
                document.getElementsByClassName('main-panel')[0].appendChild($layer);
            } else if (body.classList.contains('off-canvas-sidebar')) {
                document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
            }

            setTimeout(function () {
                $layer.classList.add('visible');
            }, 100);

            $layer.onclick = function () { //asign a function
                body.classList.remove('nav-open');
                this.mobile_menu_visible = 0;
                $layer.classList.remove('visible');
                setTimeout(function () {
                    $layer.remove();
                    $toggle.classList.remove('toggled');
                }, 400);
            }.bind(this);

            body.classList.add('nav-open');
            this.mobile_menu_visible = 1;

        }
    };

    getTitle() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }

        for (var item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        return 'Dashboard';
    }


}
